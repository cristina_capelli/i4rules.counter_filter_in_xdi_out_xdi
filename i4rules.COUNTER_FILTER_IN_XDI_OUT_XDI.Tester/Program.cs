﻿using i4bal.Base.Models;
using i4bal.Base.Models.Messages;
using i4bal.DeviceAdapter;
using i4rules.Rule.Models;
using System;
using System.Collections.Generic;
using System.Threading;

namespace i4rules.COUNTER_FILTER_IN_XDI_OUT_XDI.Tester
{
    class Program
    {

        #region Fields

        private static CancellationTokenSource _cancellationTokenSource = default(CancellationTokenSource);
        #endregion

        #region Properties

        private static CancellationTokenSource CancellationTokenSource
        {
            get
            {
                if (_cancellationTokenSource == default(CancellationTokenSource))
                    _cancellationTokenSource = new CancellationTokenSource();

                return _cancellationTokenSource;
            }
        }

        #endregion

        #region Methods

        static void Main(string[] args)
        {
            Console.WriteLine("Press a key to continue...");
            Console.ReadLine();

            Broker daBroker = new Broker("A1", new RabbitHost { Ip = "127.0.0.1", Port = "5672", User = "I4IOTGateway", Password = "I4IOTGateway" }, null);
                
            List<Channel> inputChannels = new List<Channel> { new Channel("051I00", "XDI", true), new Channel("051I01", "XDI", false) };

            VirtualChannel virtualChannel = new VirtualChannel(
                id: "051I65",
                description: "",
                type: "XDI",
                inputChannels: inputChannels,
                ruleDestination: new RabbitHost { Ip = "127.0.0.1", Port = "5672", User = "I4IOTGateway", Password = "I4IOTGateway" },
                ruleParameters: new List<Parameter> { new Parameter("LEVEL_CHANNEL", "00"), new Parameter("COUNTER_CHANNEL", "01") }
            );
            


            QueueMessage<AdapterMessage<List<DigitalInputMessage>>> messageToSend = new QueueMessage<AdapterMessage<List<DigitalInputMessage>>>();

            /* 
                INVIA UN PACCHETTO OGNI 10 SECONDI PER VERIFICARE CHE LA REGOLA CAMPIONI CORRETTAMENTE
                AD OGNI INVIO MANDA UNA LISTA DI 2 XDI 
            */

            #region Invio 1 - 0 0 

            DigitalInputMessage di1 = new DigitalInputMessage()
            {
                Channel = "051I00",
                Level = false
            };

            DigitalInputMessage di2 = new DigitalInputMessage()
            {
                Channel = "051I01",
                Counter = 2
            };


            messageToSend.Topic = "051I65";
            messageToSend.Content = new AdapterMessage<List<DigitalInputMessage>> { Content = new List<DigitalInputMessage> { di1, di2 } };
            daBroker.PublishToRule("051I65", messageToSend);

            Thread.Sleep(10000);

            #endregion

            #region Invio 2 - 0 1 

            di1 = new DigitalInputMessage()
            {
                Channel = "051I00",
                Level = false
            };

            di2 = new DigitalInputMessage()
            {
                Channel = "051I01",
                Level = true
            };
            
            messageToSend.Topic = "051I65";
            messageToSend.Content = new AdapterMessage<List<DigitalInputMessage>> { Content = new List<DigitalInputMessage> { di1, di2 } };
            daBroker.PublishToRule("051I65", messageToSend);

            Thread.Sleep(10000);

            #endregion

            #region Invio 3 - 0 1 

            di1 = new DigitalInputMessage()
            {
                Channel = "051I00",
                Level = false
            };

            di2 = new DigitalInputMessage()
            {
                Channel = "051I01",
                Level = true
            };

            messageToSend.Topic = "051I65";
            messageToSend.Content = new AdapterMessage<List<DigitalInputMessage>> { Content = new List<DigitalInputMessage> { di1, di2 } };
            daBroker.PublishToRule("051I65", messageToSend);

            Thread.Sleep(10000);

            #endregion

            #region Invio 4 - 0 0 

            di1 = new DigitalInputMessage()
            {
                Channel = "051I00",
                Level = false
            };

            di2 = new DigitalInputMessage()
            {
                Channel = "051I01",
                Level = false
            };
            
            messageToSend.Topic = "051I65";
            messageToSend.Content = new AdapterMessage<List<DigitalInputMessage>> { Content = new List<DigitalInputMessage> { di1, di2 } };
            daBroker.PublishToRule("051I65", messageToSend);

            #endregion

            Console.ReadLine();
        }

        #endregion
    }
}