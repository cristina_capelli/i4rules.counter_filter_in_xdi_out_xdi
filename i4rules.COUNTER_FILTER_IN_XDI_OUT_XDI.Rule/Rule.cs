﻿using i4bal.Base.Models.Messages;
using i4rules.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace i4rules.COUNTER_FILTER_IN_XDI_OUT_XDI.Rule
{
    public class Rule : BaseRule
    {
        #region Fields
        #endregion

        #region Properties
        #endregion

        #region Constructor

        public Rule(string ruleName, string virtualChannelId, string startDateTime)
             : base(ruleName, virtualChannelId, startDateTime)
        {
            try
            {
                // Do custom rule initialization here

                ForcedTrace($"Subscribing to messages coming from rules manager");
                Broker.SubscribeToIncomingMessages(virtualChannelId, RulesConsumer_Handler);

                ForcedTrace($"Sending keep alive");
                SendKeepAlive();
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        #endregion

        #region Methods

        protected override void Compute(List<BaseAdapterMessage> input)
        {
            bool level = false;
            int counter = 0;

            // Reads Rule parameters to define DI type to read
            string levelChannel = VirtualChannel.RuleParameters.Find(p => p.Name.Equals("LEVEL_CHANNEL")).Value.ToString();
            string counterChannel = VirtualChannel.RuleParameters.Find(p => p.Name.Equals("COUNTER_CHANNEL")).Value.ToString();

            DateTime executionTime = DateTime.Now;
            QueueMessage<AdapterMessage<List<DigitalInputMessage>>> messageReturn = new QueueMessage<AdapterMessage<List<DigitalInputMessage>>>();

            for(int i = 0; i<2; i++)
            { 
                BaseAdapterMessage ba = input.ElementAt(i);
                string channel = ba.Channel;

                DigitalInputMessage xdi = (DigitalInputMessage)ba;
                
                if (channel.Equals(levelChannel))
                {
                    level = xdi.Level;
                }
                else if (channel.Equals(counterChannel))
                {
                    counter = xdi.Counter;                    
                }                
            }

            List<DigitalInputMessage> xdiList = new List<DigitalInputMessage>();

            xdiList.Add(new DigitalInputMessage
            {
                Channel = VirtualChannel.Id,
                Timestamp = executionTime.ToString("yyyyMMddHHmmssffff"),
                Level = level,
                Description = "",
                Counter = level ? counter : 0,
                RaiseCycleTime = 0,
                TimeElapsedSinceLastRaise = 0,
                FallCycleTime = 0,
                TimeElapsedSinceLastFall = 0
            });

            messageReturn.Topic = "XXI";
            messageReturn.Content = new AdapterMessage<List<DigitalInputMessage>> { Content = xdiList };

            Broker.PublishToRulesManager(messageReturn);
        }

       
        public void Test(List<BaseAdapterMessage> input)
        {
            Compute(input);
        }

        #endregion
    }
}
