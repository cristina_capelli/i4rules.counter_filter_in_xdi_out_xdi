﻿using System;

namespace i4rules.COUNTER_FILTER_IN_XDI_OUT_XDI.Rule
{
    class Program
    {
        #region Methods

        static void Main(string[] args)
        {
            if (args.Length < 3)
                throw new ArgumentException("Mandatory parameters to start rule: rule name, virtual channel identifier and instant time of gateway start");

            COUNTER_FILTER_IN_XDI_OUT_XDI.Rule.Rule rule = new COUNTER_FILTER_IN_XDI_OUT_XDI.Rule.Rule(args[0], args[1], args[2]);
        }

        #endregion
    }
}
